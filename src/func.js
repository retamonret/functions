const getSum = (str1, str2) => {
  if (typeof str1 != "string" || typeof str2 != "string")
    return false;
  str1 = (str1 == "") ? 0 : str1;
  str2 = (str2 == "") ? 0 : str2;
  let num1 = parseInt(str1);
  let num2 = parseInt(str2);
  if (isNaN(num1) || isNaN(num2)) {
    return false;
  }
  return (num1 + num2).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let result = {
    Post: 0,
    comments: 0,
  }
  for (let p = 0; p < listOfPosts.length; p++) {
    if (listOfPosts[p].author == authorName)
      result.Post++;
    if (!listOfPosts[p].hasOwnProperty('comments')) continue;

    for (let c = 0; c < listOfPosts[p].comments.length; c++) {
      if (listOfPosts[p].comments[c].author == authorName)
        result.comments++;
    }
  }
  return "Post:" + result.Post + ",comments:" + result.comments;
}

const tickets=(people)=> {
  let cash = [0, 0];
  for (let i = 0; i < people.length; i++) {
    switch (people[i]) {
      case 25: cash[0]++; break;
      case 50: cash[1]++; break;
    }
    if (people[i] >= 50) {
      if (people[i] == 50) {
        cash[0]--;
        if (cash[0] < 0)
          return 'NO';
      }
      else {
        if (cash[1] > 0 && cash[0] > 0) {
          cash[1]--;
          cash[0]--;
        }
        else if (cash[0] > 2) {
          cash[0] -= 3;
        }
        else
          return 'NO';
      }
    }
  }
  return 'YES';
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
